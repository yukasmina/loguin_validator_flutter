import 'dart:async';

class LoginBloc {
  final _emailController = StreamController<String>.broadcast(); //el broadcas siver para que varias instancoas lo escuche..
  final _passwordController = StreamController<String>.broadcast();

  //para recuperar datos en el stream
  Stream<String> get emailStream => _emailController.stream;

  Stream<String> get passwordStream => _passwordController.stream;

  //Insertar valores al stream
  Function(String) get changeEmail => _emailController.sink.add;

  Function(String) get changePassword => _passwordController.sink.add;

  //metodo paara serrar los stream
  dispose() {
    _emailController?.close();
    _passwordController?.close();
  }
}
