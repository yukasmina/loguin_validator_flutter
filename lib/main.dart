import 'package:flutter/material.dart';
import 'package:loguin_preferences/src/bloc/provider.dart';
import 'package:loguin_preferences/src/page/loguin_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Provider(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Formulario validate',
        initialRoute: 'login',
        routes: {
          'login': (_) => LoguinPage(),
        },
      ),
    );
  }
}
